package it.easysoft;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portlet.PortletPreferencesFactoryUtil;

public class ConfigAction implements ConfigurationAction {
	
	private static final String confPage = "/html/flexsliderportlet/config.jsp";

	@Override
	public void processAction(PortletConfig portletConfig,
			ActionRequest actionRequest, ActionResponse actionResponse)
			throws Exception {
		
		String portletResource = ParamUtil.getString(actionRequest,	"portletResource");

		PortletPreferences pref = PortletPreferencesFactoryUtil.getPortletSetup(actionRequest, portletResource);

		long folderId = ParamUtil.getLong(actionRequest, "folderId");
		String animationType = ParamUtil.getString(actionRequest, "animation-type");
		boolean captions = ParamUtil.getBoolean(actionRequest, "captions");
		boolean links = ParamUtil.getBoolean(actionRequest, "links");
		boolean controlNav = ParamUtil.getBoolean(actionRequest, "control-nav");
		String pluginName = ParamUtil.getString(actionRequest, "plugin-name");
		pref.setValue("root-folder", StringUtil.valueOf(folderId));
		pref.setValue("plugin-name", pluginName);
		pref.setValue("animation-type", animationType);
		pref.setValue("captions", String.valueOf(captions));
		pref.setValue("links", String.valueOf(links));
		pref.setValue("control-nav", String.valueOf(controlNav));
		
		pref.store();
		
		SessionMessages.add(actionRequest, portletConfig.getPortletName() + ".doConfigure");
		
	}

	@Override
	public String render(PortletConfig portletConfig,
			RenderRequest renderRequest, RenderResponse renderResponse)
			throws Exception {
		String contextPath = renderRequest.getContextPath();
		return contextPath +  confPage;
	}

}
