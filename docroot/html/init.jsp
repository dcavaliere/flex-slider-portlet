<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>

<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.dao.orm.QueryUtil"%>
<%@page import="com.liferay.portal.model.Image"%>
<%@page import="com.liferay.portal.service.ImageLocalServiceUtil"%>
<%@page import="com.liferay.portal.security.permission.ActionKeys"%>

<%@page import="com.liferay.portlet.imagegallery.model.IGImage"%>
<%@page import="com.liferay.portlet.imagegallery.model.IGFolderConstants"%>
<%@page import="com.liferay.portlet.imagegallery.service.IGImageLocalServiceUtil"%>
<%@page import="com.liferay.portlet.imagegallery.service.IGFolderLocalServiceUtil"%>
<%@page import="it.easysoft.FlexSliderPortlet"%>

<%@page import="com.liferay.portlet.imagegallery.model.IGFolder"%>

<portlet:defineObjects/>
<liferay-theme:defineObjects/>
<%

long companyId = themeDisplay.getCompanyId();
long groupId = themeDisplay.getScopeGroupId();
long layoutId = themeDisplay.getLayout().getLayoutId();

long rootFolderId = Long.valueOf(portletPreferences.getValue("root-folder", "0"));

String pluginName = portletPreferences.getValue("plugin-name", "flexslider");

boolean captions = Boolean.valueOf(portletPreferences.getValue("captions", "true"));
boolean links = Boolean.valueOf(portletPreferences.getValue("links", "true"));

String animationType = portletPreferences.getValue("animation-type", "slide");

String themeFile = portletPreferences.getValue("theme-css", "flexslider.css");

boolean controlNav = Boolean.valueOf(portletPreferences.getValue("control-nav", "true"));

List<IGImage> images = new ArrayList<IGImage>();
if (rootFolderId == 0) {
	images = IGImageLocalServiceUtil.getImages(groupId, IGFolderConstants.DEFAULT_PARENT_FOLDER_ID);
} else {
	images = IGImageLocalServiceUtil.getImages(groupId, rootFolderId);
}

String contentPath = request.getContextPath();
%>