<%--
/**
* Copyright (c) 2000-2010 Liferay, Inc. All rights reserved.
*
* This library is free software; you can redistribute it and/or modify it under
* the terms of the GNU Lesser General Public License as published by the Free
* Software Foundation; either version 2.1 of the License, or (at your option)
* any later version.
*
* This library is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
* details.
*/
--%>

<%@page import="com.liferay.portal.kernel.util.StringUtil"%>
<%@ include file="../init.jsp" %>

<style>
<!--

-->

.lfr-menu-list {
  z-index: 2000;
}

.portlet-borderless-bar {
  z-index: 1500;
}
</style>

<%

String viewPah = String.format("/html/flexsliderportlet/sliders/%s/view.jsp", pluginName);

%>


<liferay-util:include page="<%= viewPah %>" 
	servletContext="<%= this.getServletContext() %>" />