<%@page import="com.liferay.portal.kernel.util.File"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="com.liferay.portlet.PortletPreferencesFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ include file="../init.jsp" %>
<%
String portletResource = ParamUtil.getString(request,"portletResource");

if (Validator.isNotNull(portletResource)) {
	portletPreferences = PortletPreferencesFactoryUtil
			.getPortletSetup(request, portletResource);
}

String rootFolder = portletPreferences.getValue("root-folder", "0");

boolean enablecaptions = Boolean.valueOf(portletPreferences.getValue("captions", "true"));

String _animationType = portletPreferences.getValue("animation-type", "slide");

boolean _controlNav = Boolean.valueOf(portletPreferences.getValue("control-nav", "true"));

long _rootFolderId = Long.valueOf(rootFolder);

List<IGFolder> folders = IGFolderLocalServiceUtil.getCompanyFolders(company.getCompanyId(),
		QueryUtil.ALL_POS, QueryUtil.ALL_POS);
%>

<style>
<!--

-->
.aui-panel-content.aui-dialog-content.aui-widget-stdmod {
  z-index: 2002;
}

</style>

<liferay-ui:header title="preferences"/>
<liferay-portlet:actionURL portletConfiguration="true" var="savePreferencesURL" />
<aui:form action="<%=savePreferencesURL.toString()%>" name="PREFFM" method="POST">

<aui:fieldset label="plugin">
	<!-- onChange='<%= renderResponse.getNamespace() + "loadOptions(this)" %>' -->
	<aui:select name="plugin-name" >
		<liferay-portlet:renderURL var="flexSliderURL">
			<liferay-util:param name="jspPage" 
				value='<%= String.format("/html/flexsliderportlet/sliders/%s/config.jsp", "flexslider") %>'/>
		</liferay-portlet:renderURL>
		<aui:option value="flexslider" selected='<%= pluginName.equalsIgnoreCase("flexslider") ? true : false %>' 
			link="<%= flexSliderURL.toString() %>"> FlexSlider </aui:option>
		
		<liferay-portlet:renderURL var="daSliderURL">
			<liferay-util:param name="jspPage" 
				value='<%= String.format("/html/flexsliderportlet/sliders/%s/config.jsp", "daslider") %>'/>
		</liferay-portlet:renderURL>
		<aui:option value="daslider" selected='<%= pluginName.equalsIgnoreCase("daslider") ? true : false %>' 
			link="<%=daSliderURL.toString() %>">DaSlider</aui:option>
	</aui:select>
</aui:fieldset>

<aui:fieldset helpMessage="select the folder from which display the images" label="folder" column="25">		
		<aui:select name="folderId" label="folder-name">
			
			<aui:option value="0" selected="<%=(_rootFolderId == 0 ) ? true : false%>">
				<liferay-ui:message key="ROOT" />
			</aui:option>
			<%
				for (int i = 0; i < folders.size(); i++) {
					IGFolder folder = folders.get(i);
					boolean selected = _rootFolderId == folder.getFolderId() ? true : false;
			%>
			<aui:option value="<%=folder.getFolderId()%>" selected="<%=selected%>">
				<%=HtmlUtil.escape(folder.getName())%>
			</aui:option>
			<%
				}
			%>
		</aui:select>


	</aui:fieldset>
	<aui:fieldset label="enable-captions">
		<aui:input name="captions" type="checkbox" checked="<%= enablecaptions %>"/>	
	</aui:fieldset>
	
	<aui:fieldset label="enable-links">
		<aui:input name="links" type="checkbox" checked="<%= links %>"/>	
	</aui:fieldset>
	
	<aui:fieldset label="animation">
		<aui:select name="animation-type">
			<aui:option label="slide" value="slide" selected="<%= _animationType.equals("slide") ? true : false %>"/>
			<aui:option label="fade" value="fade" selected="<%= _animationType.equals("fade") ? true : false %>"/>
		</aui:select>
	</aui:fieldset>
	
	<aui:fieldset label="navigator">	
		<aui:input name="control-nav" type="checkbox" checked="<%= _controlNav %>"/>
	</aui:fieldset>
	
	<aui:fieldset label="theme">
		<aui:select name="theme-css">
			<%
			// add here list of files 
			%>
			<aui:option label="flexslider"/>
			<aui:option label="flexslidex"/>
		</aui:select>
	
	</aui:fieldset>
	
	<aui:button-row>
		<aui:button type="submit" />
	</aui:button-row>

</aui:form>


<aui:layout id='<%= renderResponse.getNamespace()+ "options" %>'>

</aui:layout>

<script>
/*
Liferay.provide(window, '<%= renderResponse.getNamespace() + "loadOptions" %>', function (select) {
    var A = AUI();
    A.log("loadOptions available");
    
    var selected = A.one('#'+select.id);
    
    var targetDiv = A.one('<%= "#" + renderResponse.getNamespace()+ "options" %>');
    
    var url = selected.getAttribute('link');
    
    A.log(url);
    

    targetDiv.unplug();
    
    targetDiv.plug(A.Plugin.IO, { method: 'get', uri: url});
    
}, ['aui-node', 'liferay-portlet-url']);
*/
</script>
