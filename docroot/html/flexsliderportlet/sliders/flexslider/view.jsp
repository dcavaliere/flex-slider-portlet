<%@ include file="../../../init.jsp" %>

<%

int width = 409;
int height = 400;
String jQueryEasingPath = contentPath + "/js/jquery.easing.js";
String jQueryFlexSliderPath = contentPath + "/js/flexSlider2/jquery.flexslider-min.js";
String jQueryPath = contentPath + "/js/jquery-1.7.2.min.js";

String themeCss = contentPath + "/css/" + themeFile;
%>

<style>
.flexslider {
	min-height : 400px;
	/* max-height : 500px; */
}

.flexslider .slides img {
  display: block;
  margin: 0 auto;
  max-height: 350px;
  max-width: 500px;
  width: 100%;
}

.gdl-slider-caption {
	text-align: center;
	text-shadow: 0 1px 1px gray;
}

.gdl-slider-title {  
  font-size: 22px;
  font-weight: bold;
  
}


</style>

<style type="text/css">
@import url('<%= themeCss %>') screen;
</style>

<div class="flexslider">
	<ul class="slides">
	<% for (IGImage igImage : images) { %>
		<% 
		boolean showImage = permissionChecker
		.hasPermission(igImage.getGroupId(), IGImage.class.getName()
				, igImage.getImageId(), ActionKeys.VIEW);
		Image img = ImageLocalServiceUtil.getImage(igImage.getLargeImageId());
		
		String _imgURL = themeDisplay.getPathImage() + 
				"/image_gallery?img_id=" + igImage.getLargeImageId();
		
		%>
		
		<%
			String size = "";
			String style = "";
		
		%>
		
		<li>
			<img alt="" src="<%= _imgURL %>" <%= size %>/>
				<div class="flex-caption gdl-slider-caption">
					<c:if test="<%= captions %>">
						<div class="gdl-slider-title">
							<%= igImage.getName() %>
						</div>
						<div class="slider-title-bar">
							<%= igImage.getDescription() %>
						</div>
					</c:if>
				</div>
		</li>
		
	<% } %>
	</ul>
	
</div>


<script>
AUI({
	groups: {
		'jquery': {
			//base: 'http://ajax.googleapis.com/ajax/libs',
			async : false,
			modules : {
				'jquery' : {
					fullpath : '<%= jQueryPath %>'
				},
				'jquery-easing' : {
					fullpath : '<%= jQueryEasingPath %>' 
				},
				'flex-slider' : {
					fullpath : '<%= jQueryFlexSliderPath %>'	
				}				
			}			
		}
	}
}).ready('jquery', 'jquery-easing', 'flex-slider',
function(A) { 
	
	$('.flexslider').flexslider({
		animation : '<%= animationType %>',
		controlNav : <%= controlNav %>,
		smoothHeight: true
	});	
});
</script>
