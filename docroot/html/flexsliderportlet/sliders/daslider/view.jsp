<%@ include file="../../../init.jsp" %>

<liferay-util:html-top>
<link href='http://fonts.googleapis.com/css?family=Economica:700,400italic' rel='stylesheet' type='text/css'>
<noscript>
	<link rel="stylesheet" type="text/css" href='<%=contentPath + "css/nojs.css" %>' />
</noscript>
</liferay-util:html-top>

<%

int width = 256;
int height = 256;
String jQueryEasingPath = contentPath + "/js/jquery.easing.js";
String jQueryDaSliderPath = contentPath + "/js/daslider/jquery.cslider.js";
String jQueryModernizrPath = contentPath + "/js/daslider/modernizr.custom.28468.js";
String jQueryPath = contentPath + "/js/jquery-1.7.2.min.js";

String themeCss = String.format( contentPath + "/css/daslider/%s.css", pluginName );

String imgBasePath = contentPath + "/css/images";
%>
<style>
/* Slide in from the right*/
.da-slide-fromright h2{
    animation: fromRightAnim1 0.6s ease-in 0.8s both;
}
.da-slide-fromright p{
    animation: fromRightAnim2 0.6s ease-in 0.8s both;
}
.da-slide-fromright .da-link{
    animation: fromRightAnim3 0.4s ease-in 1.2s both;
}
.da-slide-fromright .da-img{
    animation: fromRightAnim4 0.6s ease-in 0.8s both;
}
 
/* Adjust animations for different behavior of each element: */
@keyframes fromRightAnim1{
    0%{ left: 110%; opacity: 0; }
    100%{ left: 10%; opacity: 1; }
}
@keyframes fromRightAnim2{
    0%{ left: 110%; opacity: 0; }
    100%{ left: 10%; opacity: 1; }
}
@keyframes fromRightAnim3{
    0%{ left: 110%; opacity: 0; }
    1%{ left: 10%; opacity: 0; }
    100%{ left: 10%; opacity: 1; }
}
@keyframes fromRightAnim4{
    0%{ left: 110%; opacity: 0; }
    100%{ left: 60%; opacity: 1; }
}

.da-img {
	overflow: hidden;
}

.da-img > img {
  max-height: <%= height %>px;
  max-width: <%= width %>px;
}

.da-dots {
  
  z-index: 550;
}

</style>
<style type="text/css">
@import url('<%= themeCss %>') screen;
</style>

<div class="da-slider" id="da-slider" >
 <c:if test="<%= images.size()>0 %>">
	<% for (int i=0; i<images.size(); i++) {
	IGImage igImage = images.get(i);	
	%>
	<% 
	boolean showImage = permissionChecker
	.hasPermission(igImage.getGroupId(), IGImage.class.getName()
			, igImage.getImageId(), ActionKeys.VIEW);
	Image img = ImageLocalServiceUtil.getImage(igImage.getLargeImageId());
	
	String _imgURL = themeDisplay.getPathImage() + 
			"/image_gallery?img_id=" + igImage.getLargeImageId();
	
	String cssClasses = "";
		
	if (i==0) {
		//cssClasses += "da-slide-fromright da-slide-current";
	} else {
		//cssClasses += ""
	}
	
	%>
	
	<div class='da-slide'>
		<c:if test="<%= captions %>">
			<h2><%= igImage.getName() %></h2>
			<p><%= igImage.getDescription() %></p>		
		</c:if>
		<c:if test="<%= links %>">
			<a class="da-link" href="#">Read more</a>
		</c:if>
		
		<div class="da-img"><img src="<%= _imgURL %>" ></div>
		
	</div>
	
	<% } %>
	
	</c:if>

	<c:if test="<%= images.size()==0 %>">
		<div class="da-slide da-slide-fromright da-slide-current">
			<h2>Warm welcome</h2>
			<p>When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane.</p>
			<a class="da-link" href="#">Read more</a>
			<div class="da-img"><img alt="image01" src='<%= imgBasePath + "/1.png" %>'></div>
		</div>
		<div class="da-slide da-slide-toleft">
			<h2>Easy management</h2>
			<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
			<a class="da-link" href="#">Read more</a>
			<div class="da-img"><img alt="image01" src='<%= imgBasePath + "/2.png" %>'></div>
		</div>
		<div class="da-slide da-slide-toleft">
			<h2>Revolution</h2>
			<p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
			<a class="da-link" href="#">Read more</a>
			<div class="da-img"><img alt="image01" src='<%= imgBasePath + "/3.png" %>'></div>
		</div>
		<div class="da-slide da-slide-toleft">
			<h2>Quality Control</h2>
			<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
			<a class="da-link" href="#">Read more</a>
			<div class="da-img"><img alt="image01" src='<%= imgBasePath + "/4.png" %>'></div>
		</div>
	</c:if>
	<nav class="da-arrows">
		<span class="da-arrows-prev"></span>
		<span class="da-arrows-next"></span>
	</nav>
</div>


<script>
AUI({
	groups: {
		'jquery': {
			//base: 'http://ajax.googleapis.com/ajax/libs',
			async : false,
			modules : {
				'jquery' : {
					fullpath : '<%= jQueryPath %>'
				},
				'da-slider' : {
					fullpath : '<%= jQueryDaSliderPath %>'	
				},
				'modernizr' : { 
					fullpath : '<%= jQueryModernizrPath %>'
				}
			}			
		}
	}
}).ready('jquery',  'da-slider', 'modernizr',
function(A) { 
	
	
	$('#da-slider').cslider();
		
		
});
</script>